﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentSys.Models
{
    public class Confirm
    {
        public int ServiceId { get; set; }
        public string PaymentId { get; set; }
    }
}
