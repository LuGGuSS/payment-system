﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace PaymentSys.Models
{
    public class UserValidate
    {
        public static bool Login(string Login, string Password)
        {
            using (PaymentSystemEntities context = new PaymentSystemEntities())
            {
                var entity = context.Accounts.FirstOrDefault(user =>
                user.Login.Equals(Login));

                if (entity != null)
                {
                    byte[] passHash = Convert.FromBase64String(entity.Password_Hash);

                    var PBK = new Rfc2898DeriveBytes(Password, Convert.FromBase64String(entity.Salt), 10000);

                    byte[] passCheckSumm = PBK.GetBytes(20);

                    int ok = 1;

                    for (var i = 0; i < 20; i++)
                        if (passCheckSumm[i] != passHash[i])
                            ok = 0;
                    PBK.Dispose();
                    if (ok == 1)
                        return true;
                    else
                        return false;

                }
                else
                {
                    return false;
                }
            }
        }
    }
}