﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PaymentSys.Models
{
    [Serializable]
    public class Request
    {
        public DateTime DateTime { get; set; }
        public string Sign { get; set; }
        public Payment Payment { get; set; }
        public Confirm Confirm { get; set; }
    }
}