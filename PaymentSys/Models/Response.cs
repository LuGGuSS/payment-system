﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentSys.Models
{
    public class Response
    {
        public int StatusCode;
        public string StatusDetail { get; set; }
        public DateTime DateTime { get; set; }
        public string Sign { get; set; }
        public string PaymentId { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
