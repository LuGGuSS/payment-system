﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;

using PaymentSys.Models;
namespace PaymentSys.Controllers
{
    public class PaymentController : ApiController
    {
        //enabling logger
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        [Route("")]
        [BasicAuthentication]
        [HttpPost]
        public HttpResponseMessage PaymentPostActions([FromBody] Request request)
        {
            try
            {
                if (request != null)
                {
                    if (request.Payment != null || request.Confirm != null)
                    {
                            if (request.Payment != null)
                            {
                                return CreatePayment(request);
                            }
                            else if (request.Confirm != null)
                            {
                                return ConfirmPayment(request);
                            }
                            else
                            {
                                Response responseContent = new Response()
                                {
                                    StatusCode = -4,
                                    DateTime = DateTime.Now,
                                    Sign = string.Empty,
                                    StatusDetail = "something went wrong"
                                };

                                Logger.Info($"Bad request: {request.ToString()}");

                                return Request.CreateResponse(HttpStatusCode.BadRequest, responseContent);
                            }
                    } 
                    else
                    {
                        Response responseContent = new Response()
                        {
                            StatusCode = -4,
                            DateTime = DateTime.Now,
                            Sign = string.Empty,
                            StatusDetail = "Bad request body"
                        };

                        Logger.Info($"Bad request: {request.ToString()}");

                        return Request.CreateResponse(HttpStatusCode.BadRequest, responseContent);
                    }
                }
                else
                {
                    Response responseContent = new Response()
                    {
                        StatusCode = -4,
                        DateTime = DateTime.Now,
                        Sign = string.Empty,
                        StatusDetail = "Bad request body"
                    };

                    Logger.Info($"Bad request: {request.ToString()}");

                    return Request.CreateResponse(HttpStatusCode.BadRequest, responseContent);
                }

            }
            catch (Exception ex)
            {
                Response responseContent = new Response()
                {
                    DateTime = DateTime.Now,
                    Sign = string.Empty,
                    StatusCode = -5,
                    StatusDetail = ex.ToString()
                };

                Logger.Error(ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContent);
            }
        }

        [NonAction]
        HttpResponseMessage CreatePayment(Request request)
        {
            using (PaymentSystemEntities context = new PaymentSystemEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;

                var accountEntity = context.Accounts.FirstOrDefault(account =>
                                account.AccountName == request.Payment.Account);
                if (accountEntity != null)
                {
                    if (accountEntity.Balance >= request.Payment.Amount)
                    {
                        Payment processedPayment = new Payment()
                        {
                            Amount = request.Payment.Amount,
                            ServiceId = request.Payment.ServiceId,
                            OrderId = request.Payment.OrderId,
                            Account = request.Payment.Account,
                            OrderDate = DateTime.Now,
                            PaymentId = Guid.NewGuid().ToString(),
                            Confirmed = false
                        };

                        accountEntity.Balance -= request.Payment.Amount;

                        context.Payments.Add(processedPayment);
                        context.SaveChanges();

                        Response responseContent = new Response()
                        {
                            StatusCode = 0,
                            DateTime = DateTime.Now,
                            Sign = string.Empty,
                            StatusDetail = "Payment was created",
                            PaymentId = processedPayment.PaymentId
                        };

                        Logger.Info($"Payment created: PaymentId:{processedPayment.PaymentId}, AccountName: {processedPayment.Account}, ServiceId: {processedPayment.ServiceId}, Ammount: {processedPayment.Amount}, OrderId: {processedPayment.OrderId}");

                        return Request.CreateResponse(HttpStatusCode.OK, responseContent);
                    }
                    else
                    {
                        Response responseContent = new Response()
                        {
                            StatusCode = -1,
                            DateTime = DateTime.Now,
                            Sign = string.Empty,
                            StatusDetail = "Not enough funds"
                        };

                        Logger.Info($"Payment was not created due to insufficient funds: AccountName: {request.Payment.Account}, ServiceId: {request.Payment.ServiceId}, Ammount: {request.Payment.Amount}, OrderId: {request.Payment.OrderId}");

                        return Request.CreateResponse(HttpStatusCode.BadRequest, responseContent);
                    }
                }
                else
                {
                    Response responseContent = new Response()
                    {
                        StatusCode = -3,
                        DateTime = DateTime.Now,
                        Sign = string.Empty,
                        StatusDetail = "No such account"
                    };

                    Logger.Info($"No account was found with account name :{request.Payment.Account}");

                    return Request.CreateResponse(HttpStatusCode.NotFound, responseContent);
                }
            }
        }

        [NonAction]
        HttpResponseMessage ConfirmPayment(Request request)
        {
            using (PaymentSystemEntities context = new PaymentSystemEntities())
            {
                context.Configuration.LazyLoadingEnabled = false;

                var paymentEntity = context.Payments.FirstOrDefault(payment =>
                                    payment.PaymentId == request.Confirm.PaymentId &&
                                    payment.ServiceId == request.Confirm.ServiceId);
                if (paymentEntity != null)
                {
                    paymentEntity.Confirmed = true;
                    context.SaveChanges();

                    Response responseContent = new Response()
                    {
                        StatusCode = 0,
                        DateTime = DateTime.Now,
                        Sign = string.Empty,
                        StatusDetail = "Payment confirmed",
                        OrderDate = paymentEntity.OrderDate.Value
                    };

                    Logger.Info($"Payment was confirmed: PaymentId: {paymentEntity.PaymentId}, ServiceId: {paymentEntity.ServiceId}");

                    return Request.CreateResponse(HttpStatusCode.OK, responseContent);
                }
                else
                {
                    Response responseContent = new Response()
                    {
                        StatusCode = -2,
                        DateTime = DateTime.Now,
                        Sign = string.Empty,
                        StatusDetail = "No such payment"
                    };

                    Logger.Info($"Payment was not found: PaymentId: {paymentEntity.PaymentId}, ServiceId: {paymentEntity.ServiceId}");

                    return Request.CreateResponse(HttpStatusCode.NotFound, responseContent);
                }
            }
        }

        // for testing purposes
        //sample xml body
        //<Account>
        //    <Login>222</Login>
        //    <Password_Hash>222</Password_Hash>
        //</Account>
        [Route("api/Payment/QuickReg")]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage QuickReg ([FromBody] Account accountToReg)
        {
            try
            {

                using (PaymentSystemEntities dbContext = new PaymentSystemEntities())
                {

                    byte[] salt_temp;
                    //generating salt
                    new RNGCryptoServiceProvider().GetBytes(salt_temp = new byte[16]);

                    //generating password hash
                    Rfc2898DeriveBytes PBKDF2 = new Rfc2898DeriveBytes(accountToReg.Password_Hash, salt_temp, 10000);

                    byte[] hash = PBKDF2.GetBytes(20);


                    string savedPasswordHash = Convert.ToBase64String(hash);
                    string savedSalt = Convert.ToBase64String(salt_temp);


                    Account entity = new Account()
                    {
                        AccountName = Guid.NewGuid().ToString(),
                        Balance = 100,
                        Login = accountToReg.Login,
                        Salt = savedSalt,
                        Password_Hash = savedPasswordHash
                    };



                    dbContext.Accounts.Add(entity);
                    dbContext.SaveChanges();

                    Logger.Info($"Account was created: Login: {accountToReg.Login}, Password: {accountToReg.Password_Hash}");

                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                Response responseContent = new Response()
                {
                    DateTime = DateTime.Now,
                    Sign = string.Empty,
                    StatusCode = -3,
                    StatusDetail = ex.ToString()
                };

                Logger.Error(ex);

                return Request.CreateResponse(HttpStatusCode.InternalServerError, responseContent);
            }
        }
    }
}
